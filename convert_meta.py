#!/usr/bin/python3
#
# in build/org.fdroid.fdroid/
# ./convert_meta.py app/src/main/res/*/strings.xml
import os
from pathlib import Path
from sys import argv

import yaml
from fdroidserver import common

antiFeatures = {
    "antiads": "Ads",
    "antidisabledalgorithm": "DisabledAlgorithm",
    "antiknownvuln": "KnownVuln",
    "antinonfreead": "NonFreeAdd",
    "antinonfreeassets": "NonFreeAssets",
    "antinonfreedep": "NonFreeDep",
    "antinonfreenet": "NonFreeNet",
    "antinosourcesince": "NoSourceSince",
    "antinsfw": "NSFW",
    "antitrack": "Tracking",
    "antiupstreamnonfree": "UpstreamNonFree",
}


def sort_dict(dct):
    return {
        k: {
            key: dct[k][key] for key in ("name", "description", "icon") if key in dct[k]
        }
        for k in sorted(dct)
    }


def main():
    for path in argv[1:]:
        categories = {}
        antifs = {}
        xml = common.parse_xml(path)
        locale = path.split("/")[-2].removeprefix("values-")
        # skip symlinks for old Android versions, see:
        # https://gitlab.com/fdroid/fdroiddata/-/merge_requests/11086#note_1006108182
        if locale == 'iw' or locale == 'in':
            continue
        if locale == "values":
            locale = ""
        for string in xml.findall(".//string"):
            name = string.attrib["name"]
            if name.startswith("category_"):
                category = name.removeprefix("category_")
                categories[category] = {}
                categories[category]["name"] = string.text
                icon = f"app/src/main/res/drawable/{name}.png".lower()
                if not locale and os.path.exists(icon):
                    categories[category]["icon"] = os.path.basename(icon)
            elif name.startswith("anti"):
                key = name.removesuffix("list")
                if key.endswith("_key") or key in (
                    "antifeatures",
                    "antifeatureswarning",
                    "antinosource",
                    "antiothers",
                ):
                    continue
                key = antiFeatures[key]
                if key not in antifs:
                    antifs[key] = {}
                if name.endswith("list"):
                    antifs[key]["description"] = string.text
                else:
                    name_key = "name"
                    if name == "antinsfw":
                        name_key = "description"
                        antifs[key]["name"] = "NSFW"
                    antifs[key][name_key] = string.text
                    aff = name.removeprefix("anti")
                    if aff == "track":
                        aff = "tracking"
                    if aff == "nonfreead":
                        aff = "nonfreeadd"

                    icon = f"app/src/main/res/drawable/ic_antifeature_{aff}.xml"
                    if not locale and os.path.exists(icon):
                        antifs[key]["icon"] = os.path.basename(icon)

        antifs = sort_dict(antifs)
        categories = sort_dict(categories)

        if antifs or categories:
            os.makedirs(f"conf/{locale}", exist_ok=True)

        if antifs:
            Path(f"conf/{locale}/antiFeatures.yml").write_text(
                yaml.dump(antifs, allow_unicode=True, sort_keys=False), encoding="utf-8"
            )
        if categories:
            Path(f"conf/{locale}/categories.yml").write_text(
                yaml.dump(categories, allow_unicode=True, sort_keys=False), encoding="utf-8"
            )


if __name__ == "__main__":
    main()
