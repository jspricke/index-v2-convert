#!/usr/bin/python3
import calendar
import collections
import datetime
import hashlib
import json
import os
from pathlib import Path
from sys import argv


def sha256sum(filename):
    """Calculate the sha256 of the given file."""
    sha = hashlib.sha256()
    with open(filename, 'rb') as f:
        while True:
            t = f.read(16384)
            if len(t) == 0:
                break
            sha.update(t)
    return sha.hexdigest()


def dict_diff(source, target):
    if not isinstance(target, dict) or not isinstance(source, dict):
        return target

    result = {key: None for key in source if key not in target}

    for key, value in target.items():
        if key not in source:
            result[key] = value
        elif value != source[key]:
            result[key] = dict_diff(source[key], value)

    return result


def file_entry(filename, hashType=None, hsh=None, size=None):
    meta = {}
    meta["name"] = filename
    if hsh:
        meta[hashType] = hsh
    if hsh != "sha256":
        meta["sha256"] = sha256sum(filename)
    if size:
        meta["size"] = size
    else:
        meta["size"] = os.stat(filename).st_size
    return meta


def main():
    def _index_encoder_default(obj):
        if isinstance(obj, set):
            return sorted(list(obj))
        if isinstance(obj, datetime):
            # Java prefers milliseconds
            # we also need to account for time zone/daylight saving time
            return int(calendar.timegm(obj.timetuple()) * 1000)
        if isinstance(obj, dict):
            d = collections.OrderedDict()
            for key in sorted(obj.keys()):
                d[key] = obj[key]
            return d
        raise TypeError(repr(obj) + " is not JSON serializable")

    repodir = "./"
    entry = {}

    entry["version"] = 20001

    indexes = [json.loads(Path(fn).read_text(encoding="utf-8")) for fn in argv[1:]]
    index = indexes.pop()

    entry["timestamp"] = index["repo"]["timestamp"]
    entry["index"] = file_entry(argv[-1])
    entry["index"]["numPackages"] = len(index.get("packages", []))

    for diff in Path().glob("{}/diff/*.json".format(repodir)):
        diff.unlink()

    entry["diffs"] = {}
    for old in indexes:
        diff_name = str(old["repo"]["timestamp"]) + ".json"
        diff_file = os.path.join(repodir, "diff", diff_name)
        diff = dict_diff(old, index)
        if not os.path.exists(os.path.join(repodir, "diff")):
            os.makedirs(os.path.join(repodir, "diff"))
        with open(diff_file, "w", encoding="utf-8") as fp:
            json.dump(diff, fp, default=_index_encoder_default, indent=2, ensure_ascii=False)

        entry["diffs"][old["repo"]["timestamp"]] = file_entry(diff_file)
        entry["diffs"][old["repo"]["timestamp"]]["numPackages"] = len(diff.get("packages", []))

    json_name = "entry.json"
    index_file = os.path.join(repodir, json_name)
    with open(index_file, "w", encoding="utf-8") as fp:
        json.dump(entry, fp, default=_index_encoder_default, indent=2, ensure_ascii=False)


if __name__ == "__main__":
    main()
